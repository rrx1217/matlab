function Q=intquad(n)
a=ones(n);
Q=[a*-1,a*exp(1);a*pi,a*1];
end